const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const jwtSchema = new Schema ({
    Username: {
        type: String,
        required: true,
        unique: true
    },
    Password: {
        type: String,
        required: true
    },
    Email: {
        type: String,
        required: true,
        unique: true
    },
    Role: {
        type: String,
        required: true
    },
    AccessToken: {
        type: String
    },
    RefToken: {
        type: String
    },
    Information: {
        type: mongoose.Types.ObjectId,
        ref: "Customer"
    }
}, {timestamps: true})

module.exports = mongoose.model("Jwt", jwtSchema);