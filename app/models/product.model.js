const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const productSchema = new Schema ({
    productId: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true,
        unique: true
    },
    imageUrl: {
        type: String,
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    size: {
        type: []
    },
    color: {
        type: []
    },
    gender: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    brand: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    promotionPrice: {
        type: Number
    },
    description: {
        type: String,
        required: true
    },
    productInformation: {
        type: String,
        required: true
    },
    materialUsed: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model("Product", productSchema);