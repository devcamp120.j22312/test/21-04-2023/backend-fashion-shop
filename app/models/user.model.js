const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    userId : {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    userName: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    birthday: {
        type: String
    },
    phone: {
        type: Number
    }, 
    gender: {
        type: String
    }, 
    address: {
        type: String
    },
    orders: {
        type: []
    },
    carts: {
        type: mongoose.Types.ObjectId,
        ref: "Order"
    },
    shoppingHistory: {
        ref: "orderId",
        required: true
    }
})

module.exports = mongoose.model("User", userSchema)