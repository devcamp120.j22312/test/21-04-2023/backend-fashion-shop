const express = require('express');
const router = express.Router();

const orderController = require('../controllers/order.controller');

router.post('/orders', orderController.createOrder);
router.get('/orders/:orderId', orderController.getOrderById);
router.get('/orders', orderController.getAllOrder);
router.get('/customer-info/:orderId', orderController.findCustomerInfoByOrderId);
router.put('/update-status', orderController.updateStatusOrder);
router.delete('/orders', orderController.deleteOrder);
router.get('/search-orders/:phoneNumber', orderController.findOrderByPhoneNumber);
router.get('/search-orders/:date', orderController.findOrderByDate);

module.exports = router;