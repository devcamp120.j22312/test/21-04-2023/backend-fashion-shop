const express = require('express');
const router = express.Router();

const userController = require('../controllers/user.controller');

router.post('/users', userController.createUser);
router.get('/users', userController.getAllUser);
router.get('/users/:userId', userController.getUserByUserId);
router.get('/users/:email', userController.getUserByEmail);
router.get('/users&orders', userController.getUserAndOrdersByUserId);
router.put('/users/:userId', userController.updateInfoUser);

module.exports = router;