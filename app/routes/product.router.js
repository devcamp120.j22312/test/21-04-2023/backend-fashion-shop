const express = require('express');

const productController = require('../controllers/product.controller')

const router = express.Router();

// Create Product
router.post('/products', productController.createProduct);

// Get All Product
router.get('/products', productController.getAllProduct);

// Get Product By Id 
router.get('/products/:productId', productController.getProductById);

// Update Product
router.put('/products/:productId', productController.updateProductById);

// Delete Product
router.delete('/products/:productId', productController.deleteProductById)

module.exports = router;