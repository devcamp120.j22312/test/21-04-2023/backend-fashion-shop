const mongoose = require('mongoose');
const productModel = require('../models/product.model');


const createProduct = (request, response) => {
    const body = request.body;

    if (!body.name) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Name Product không hợp lệ"
        })
    }
    if (!body.imageUrl) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ImageUrl Product không hợp lệ"
        })
    }
    if (!body.quantity) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Quantity không hợp lệ"
        })
    }
    if (!body.gender) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Gender không hợp lệ"
        })
    }
    if (!body.category) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Category không hợp lệ"
        })
    }
    if (!body.brand) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Brand Product không hợp lệ"
        })
    }
    if (!body.price) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Price Product không hợp lệ"
        })
    }
    if (!body.description) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Description Product không hợp lệ"
        })
    }
    if (!body.productInformation) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Product Information không hợp lệ"
        })
    }
    if (!body.materialUsed) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Material Used không hợp lệ"
        })
    }

    const newProduct = {
        productId: body.productId,
        name: body.name,
        imageUrl: body.imageUrl,
        quantity: body.quantity,
        size: body.size,
        color: body.color,
        gender: body.gender,
        category: body.category,
        brand: body.brand,
        price: body.price,
        promotionPrice: body.promotionPrice,
        description: body.description,
        productInformation: body.productInformation,
        materialUsed: body.materialUsed
    }

    productModel.create(newProduct, (err, data) => {
        if (err) {
            return response.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return response.status(201).json({
            status: "Create product successfully",
            data: data
        })
    })
}

const getAllProduct = (request, response) => {
    productModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all product successfully",
            Product: data
        })
    })
}

const getProductById = (request, response) => {
    const productId = request.query.productId;

    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: 'Product Id is not valid'
        })
    }

    productModel.findById(productId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get product by id successfully",
            data: data
        })
    })
}

const updateProductById = (request, response) => {
    const productId = request.query.productId;
    const body = request.body;

    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Product Id is not valid'
        })
    }

    let updateProduct = {
        productId: body.productId,
        name: body.name,
        imageUrl: body.imageUrl,
        quantity: body.quantity,
        size: body.size,
        color: body.color,
        gender: body.gender,
        category: body.category,
        brand: body.brand,
        price: body.price,
        promotionPrice: body.promotionPrice,
        description: body.description,
        productInformation: body.productInformation,
        materialUsed: body.materialUsed
    }

    productModel.findByIdAndUpdate(productId, updateProduct, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: 'Update product successfully',
            data: data
        })
    })
}

const deleteProductById = (request, response) => {
    const productId = request.query.productId;

    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Product Id is not valid"
        })
    }

    productModel.findByIdAndDelete(productId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: 'Delete product successfully'
        })
    })
}

module.exports = {
    createProduct,
    getAllProduct,
    getProductById,
    updateProductById,
    deleteProductById,

}