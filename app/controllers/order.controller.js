
const mongoose = require('mongoose');
const orderModel = require('../models/order.model');
const userModel = require('../models/user.model');

const createOrder = (req, res) => {
    const body = req.body;
    const userId = req.query.userId;

    const newOrder = {
        _id: mongoose.Types.ObjectId(),
        status: "Ordered",
        order: body.order
    }

    userModel.findOne({ userId: userId }, (errorId, idExist) => {
        if (errorId) {
            return res.status(500).json({
                status: "Internal server error",
                message: errorId.message
            })
        }
        orderModel.create(newOrder, (error, data) => {
            if (error) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            userModel.findOneAndUpdate({ userId: userId }, {
                $push: {
                    carts: data._id
                }
            }, { new: true }, (err, updateCustomer) => {
                if (err) {
                    return res.status(500).json({
                        status: "Internal server error",
                        message: err.message
                    })
                }
                return res.status(201).json({
                    status: "Create order with user id successfully",
                    data: data
                })
            })
        })
    })

}

const getOrderById = (req, res) => {
    const orderId = req.query.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Order ID is not valid"
        })
    }

    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(201).json({
            status: "Get order by id successfully",
            data: data
        })
    })
}

const getAllOrder = (req, res) => {
    const skipNumber = req.query.skipNumber;
    const limitNumber = req.query.limitNumber;

    if (skipNumber) {
        orderModel.find()
            .skip(skipNumber)
            .limit(limitNumber)
            .exec((err, data) => {
                if (err) {
                    return res.status(500).json({
                        status: "Internal server error",
                        message: err.message
                    })
                }
                else {
                    return res.status(201).json({
                        status: "Get order successfully",
                        data: data
                    })
                }

            })
    }
    else {
        orderModel.find((error, data) => {
            if (error) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            else {
                return res.status(201).json({
                    status: "Get all orders successfully",
                    data: data
                })
            }
        })
    }
}

const findCustomerInfoByOrderId = (req, res) => {
    const orderId = req.query.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Order ID is not valid"
        })
    }

    userModel.findOne({ carts: orderId }, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        else {
            return res.status(201).json({
                status: "Get order successfully",
                data: data
            })
        }
    })
}

const updateStatusOrder = (req, res) => {
    const orderId = req.query.orderId;
    const status = req.body.status;

    orderModel.findOneAndUpdate({ _id: orderId }, { Status: status }, { new: true }, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Update order status successfully",
                data: data
            })
        }
    })
}

const deleteOrder = (req, res) => {
    const orderId = req.query.orderId;
    const userId = req.query.userId;

    orderModel.findByIdAndRemove({ _id: orderId }, { new: true }, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        userModel.findByIdAndUpdate(userId, {
            $pull: {
                carts: orderId
            }
        }, { new: true }, (err, updateCustomer) => {
            if (err) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            else {
                return res.status(201).json({
                    status: "Delete order successfully"
                })
            }
        })

    })
}

const findOrderByPhoneNumber = (req, res) => {
    const phoneNumber = req.query.phoneNumber;

    userModel.findOne({ phone: { $regex: phoneNumber } }, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        if (data == null) {
            return res.status(400).json({
                status: "Bad request",
                message: 'Phone Number is not valid'
            })
        }
        else {
            return res.status(201).json({
                status: "Find order by phone number successfully",
                data: data
            })
        }
    })
}

const findOrderByDate = (req, res) => {
    const startDate = req.query.startDate;
    const endDate = req.query.endDate;
    const skipNumber = req.query.skipNumber;
    const limitNumber = req.query.limitNumber;

    // start date < end date
    if (new Date(startDate).getTime() < new Date(endDate).getTime()) {
        orderModel.find({
            createdAt: {
                $gte: startDate,
                $lt: new Date(endDate).setDate(new Date(endDate).getDate() + 1)
            }
        })
            .skip(skipNumber)
            .limit(limitNumber)
            .exec((err, data) => {
                if (err) {
                    return res.status(500).json({
                        status: "Internal server error",
                        message: err.message
                    })
                }
                else {
                    return res.status(201).json({
                        status: "Get order successfully",
                        data: data
                    })
                }
            })
    }

    // chỉ nhập start date
    if (new Date(startDate).getTime() === new Date(endDate).getTime()) {
        orderModel.find({
            createdAt: {
                $gte: startDate,
                $lt: new Date(startDate).setDate(new Date(startDate).getDate() + 1)
            }
        })
            .skip(skipNumber)
            .limit(limitNumber)
            .exec((errorFound, data) => {
                if (errorFound) {
                    return res.status(500).json({
                        status: "Internal server error",
                        message: error.messsage
                    })
                }
                else {
                    return res.status(200).json({
                        status: "Get data order by date",
                        data: data
                    })
                }
            })
    }

    // start date > end date
    if (new Date(startDate).getTime() > new Date(endDate).getTime()) {
        orderModel.find({
            createdAt: {
                $gte: endDate,
                $lt: new Date(startDate).setDate(new Date(startDate).getDate() + 1)
            }
        })
            .skip(skipNumber)
            .limit(limitNumber)
            .exec((error, data) => {
                if (error) {
                    return res.status(500).json({
                        status: "Internal server error",
                        message: error.messsage
                    })
                }
                else {
                    return res.status(200).json({
                        status: "Get data order by date",
                        data: data
                    })
                }
            })
    }
}

module.exports = {
    createOrder,
    getOrderById,
    getAllOrder,
    findCustomerInfoByOrderId,
    updateStatusOrder,
    deleteOrder,
    findOrderByPhoneNumber,
    findOrderByDate,

}