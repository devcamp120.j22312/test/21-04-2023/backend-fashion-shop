const mongoose = require('mongoose');

const userModel = require('../models/user.model');

const createUser = (req, res) => {
    const body = req.body;

    if (!body.userName) {
        return response.status(400).json({
            status: "Bad Request",
            message: "User name không hợp lệ"
        })
    }
    if (!body.email) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Email không hợp lệ"
        })
    }

    const newUser = {
        userId: mongoose.Types.ObjectId(),
        userName: body.userName,
        email: body.email,
        birthday: body.birthday,
        phone: body.phone,
        gender: body.gender,
        address: body.address,
        orders: body.orders
    }

    userModel.create(newUser, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(201).json({
            status: "Create user successfully",
            data: data
        })
    })
}

const getAllUser = (req, res) => {
    userModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(201).json({
            status: "Get all user successfully",
            data: data
        })
    })
}

const getUserAndOrdersByUserId = (req, res) => {
    const userId = req.query.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "UserID is not valid"
        })
    }

    userModel.findOne({ UserId: userId })
        .populate("carts")
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return res.status(201).json({
                status: "Get user by id successfully",
                data: data
            })
        })
}
const getUserByUserId = (req, res) => {
    const userId = req.query.userId;

    userModel.findOne({ UserId: userId })
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return res.status(201).json({
                status: 'Get user by userid successfully',
                data: data
            })
        })
}

const getUserByEmail = (req, res) => {
    const email = req.query.email;

    userModel.findOne({ Email: email })
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return res.status(201).json({
                status: 'Get user by email successfully',
                data: data
            })
        })
}

const updateInfoUser = (req, res) => {
    const userId = req.query.userId;
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: "UserID is not valid"
        })
    }
    const updateInfo = {};
    if (body.userName !== "") {
        updateInfo.userName = body.userName;
    }
    if (body.name !== "") {
        updateInfo.name = body.name;
    }
    if (body.email !== "") {
        updateInfo.email = body.email;
    }
    if (body.birthday !== "") {
        updateInfo.birthday = body.birthday;
    }
    if (body.phone !== "") {
        updateInfo.phone = body.phone;
    }
    if (body.gender !== "") {
        updateInfo.gender = body.gender;
    }
    if (body.address !== "") {
        updateInfo.address = body.address;
    }
    if (body.orders !== "") {
        updateInfo.orders = body.orders;
    }
    userModel.findOneAndUpdate({ UserId: userId }, updateInfo, { new: true }, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(201).json({
            status: "Update user successfully",
            data: data
        })
    })
}

module.exports = {
    createUser,
    getAllUser,
    getUserByUserId,
    getUserByEmail,
    getUserAndOrdersByUserId,
    updateInfoUser,


}