// Khai báo thư viện express
const express = require('express');
const path = require('path');
const mongoose = require("mongoose");

// Khai báo port 
const port = 8800;

// Khai báo app
const app = express();

app.use(express.json());
    
mongoose.connect("mongodb://127.0.0.1:27017/Fashion_Shop", (error) => {
    if (error) throw error;
    console.log("Connect MongoDB successfully!");
})

// Import Router
const productRouter = require('./app/routes/product.router');

app.use("/api", productRouter);

// Khởi động app
app.listen(port, () => {
    console.log("App listening on port: ", port);
})